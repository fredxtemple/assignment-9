#include <stdio.h>
#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <random>
#include <string>
#include <map>
#include <iomanip>

//I can't make a convergence test that works - the something wrong with the while-loop from line 153 and onwards. 

//I have commented off my failed convergence while-loop

using namespace std;
fstream filex1;
fstream filex2;
std::random_device rd;
std::mt19937 gen(rd());

 
double f(double x1, double x2){
return (1 - x1)*(1 - x1) + 100*(x1*x1 - x2)*(x1*x1 - x2);
}

int main() {
    filex1.open("MCMC2.txt",ios::out);
    double mu1, mu2, sigma1, sigma2, P1, P2, beta;
    vector<vector<double>> x_bar(1000, vector<double>(2));
    std::uniform_real_distribution<double> randDouble1(0.01,10.); //random number generator between 0.01 - 10
    std::uniform_real_distribution<double> randDouble2(0.,1.); //random number generator between 0-1
    int i;
    double x1 = randDouble1(gen); 
    double x2 = randDouble1(gen);
    int count1 = 0;
     for (i = 0; i < 1000; i++) {
        mu1 = x1; //define the mean around this randomly chosen x
        mu2 = x2;
        sigma1 = 5.;     
        sigma2 = 5.;
        std::normal_distribution<double> d1(mu1, sigma1);
        std::normal_distribution<double> d2(mu2, sigma2);
        P1 = d1(gen);
        P2 = d2(gen);

        double y1 = P1; // select candidate step using the proposal function
        double y2 = P2;

        double alpha = f(y1, y2)/f(x1, x2); 

        if (alpha <= 1) { 
            x1 = y1; 
            x2 = y2;
            x_bar[i][0] = x1;
            x_bar[i][1] = x2;
            
            
        }
        else{

            beta = randDouble2(gen);
            if (beta <= 1/alpha) {
            	x1 = y1; 
            	x2 = y2;
            	x_bar[i][0] = x1;
            	x_bar[i][1] = x2;
            }
        }
    count1++;
    cout <<count1<<" count1 "<<endl;}
 filex2.open("MCMC2burnin.txt",ios::out);
 for (i = 0; i < 1000; i++) {
 filex2<<x_bar[i][0]<<"                       "<<x_bar[i][1]<< "                          "<<endl;
}
    

    //Burning Period complete
     int count2 = 0;
     x1 = x_bar[999][0]; //Do M-H again
     x2 = x_bar[999][1];
      for (i = 0; i < 1000; i++) {
        mu1 = x1; //define the mean around this randomly chosen x
        mu2 = x2;
        sigma1 = 5.;     
        sigma2 = 5.;
        std::uniform_real_distribution<double> randDouble1(0.01,10.); //random number generator between 0.01 - 10
        std::uniform_real_distribution<double> randDouble2(0.,1.); 
        std::normal_distribution<double> d1(mu1, sigma1);
        std::normal_distribution<double> d2(mu2, sigma2);
        P1 = d1(gen);
        P2 = d2(gen);

        double y1 = P1; // select candidate step using the proposal function
        double y2 = P2;

        double alpha = f(y1, y2)/f(x1, x2); 

        if (alpha <= 1) { 
            x1 = y1; 
            x2 = y2;
            x_bar[i][0] = x1;
            x_bar[i][1] = x2;
            
            
        }
        else{

            beta = randDouble2(gen);
            if (beta <= 1/alpha) {
            	x1 = y1; 
            	x2 = y2;
            	x_bar[i][0] = x1;
            	x_bar[i][1] = x2;
            }
        }
  
 }
 

    double mean1, mean2, dev1, dev2, sdev1, sdev2, var1, var2, var_threshold, sd1, sd2, sum1, sum2;
    sum1 = sum2 = 0.0;

    for(i = 0; i < 1000; i++) {
        sum1 = sum1 + x_bar[i][0];
        sum2 = sum2 + x_bar[i][1];
    }

    mean1 = sum1/1000.;
    mean2 = sum2/1000.;

    for(i = 0; i < 1000; i++) {
        dev1 = (x_bar[i][0] - mean1)*(x_bar[i][0] - mean1);
        dev2 = (x_bar[i][1] - mean2)*(x_bar[i][1] - mean2);
        sdev1 = sdev1 + dev1;
        sdev2 = sdev2 + dev2;
    }

    var1 = sdev1/999.0;
    var2 = sdev2/999.0;
    var_threshold = 1.e-6; // threshold variance 
    count2++;
    cout <<count2<<" count2 "<<endl;
       
      int count3, count4;
      count3 = count4 = 0;
      if ((var1 > var_threshold)  && (var2 > var_threshold)) { //convergence test
        for (i = 0; i < 1000; i++) {
            filex1<<x_bar[i][0]<<"                       "<<x_bar[i][1]<< "                          "<<endl;
        }
    count3++;
    cout <<count3<<" count3 "<<endl;
       }
}


      /*else { //Repeat M-H
       while((var1 > var_threshold)  && (var2 > var_threshold)) {
        x1 = x_bar[999][0]; 
        x2 = x_bar[999][1];
        for (i = 0; i < 1000; i++) {
        mu1 = x1; //define the mean around this randomly chosen x
        mu2 = x2;
        sigma1 = 5.;     
        sigma2 = 5.;
        std::normal_distribution<double> d1(mu1, sigma1);
        std::normal_distribution<double> d2(mu2, sigma2);
        P1 = d1(gen);
        P2 = d2(gen);

        double y1 = P1; // select candidate step using the proposal function
        double y2 = P2;

        double alpha = f(y1, y2)/f(x1, x2); 

        if (alpha <= 1) { 
            x1 = y1; 
            x2 = y2;
            x_bar[i][0] = x1;
            x_bar[i][1] = x2;
           
            
        }
        else{

            beta = randDouble2(gen);
            if (beta <= 1/alpha) {
            	x1 = y1; 
            	x2 = y2;
            	x_bar[i][0] = x1;
            	x_bar[i][1] = x2;
             }
        }
  
 
    double mean1, mean2, dev1, dev2, sdev1, sdev2, var1, var2, var_threshold, sd1, sd2, sum1, sum2;
    sum1 = sum2 = 0.0;

    for(i = 0; i < 1000; i++) {
        sum1 = sum1 + x_bar[i][0];
        sum2 = sum2 + x_bar[i][1];
    }

    mean1 = sum1/1000.;
    mean2 = sum2/1000.;

    for(i = 0; i < 1000; i++) {
        dev1 = (x_bar[i][0] - mean1)*(x_bar[i][0] - mean1);
        dev2 = (x_bar[i][1] - mean2)*(x_bar[i][1] - mean2);
        sdev1 = sdev1 + dev1;
        sdev2 = sdev2 + dev2;
    }

    var1 = sdev1/999.0;
    var2 = sdev2/999.0;
    var_threshold = 1.e-6; // threshold variance
    filex1<<x_bar[i][0]<<"    "<<x_bar[i][1]<< "  "<<endl;}
     }
   count4++;
   cout <<count4<<" count4 "<<endl;
       }
}*/

 
















